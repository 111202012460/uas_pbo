package uas_pbo;

public interface LivingThing{
    public void walk();
    public void eat();
}
